﻿using ElectronicBill.ConsoleApp.Services;
using ElectronicBill.Models;
using ElectronicBill.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ElectronicBill.ConsoleApp.Controllers
{
    public class BaseController : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged<T>(ref T currentValue, T newValue, string propertyName)
        {
            if (!newValue?.Equals(currentValue) ?? false)
            {
                currentValue = newValue;
                RaisePropertyChanged(propertyName);
            }
        }

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

    }
}
