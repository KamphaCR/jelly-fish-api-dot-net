﻿using ElectronicBill.ConsoleApp.Services;
using ElectronicBill.Models;
using ElectronicBill.Services;
using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ElectronicBill.ConsoleApp.Controllers
{
    public class MainController : BaseController
    {
        public ICommand SalirApplicationCommand { get; private set; }
        public ICommand EnviarHaciendaCommand { get; private set; }
        public ICommand AcercaDeCommand { get; private set; }
        public ICommand RevisarEnvioCommand { get; private set; }
        public ICommand NuevaFacturaCommand { get; private set; }
        public ICommand SalvarOutputResultCommand { get; private set; }
        public ICommand ReloadConfigCommand { get; private set; }
        public ICommand SaveConfigCommand { get; private set; }

        private HaciendaClient _haciendaClient;
        private FacturaElectronica _facturaElectronica;

        public string Clave
        {
            get { return _clave; }
            set { RaisePropertyChanged(ref _clave, value, nameof(Clave)); }
        }
        private string _clave;

        public MainController()
        {
            _haciendaClient = new HaciendaClient(
                    clientId: ConfigManager.Instance.Config.HaciendaClientConfig.ClientId,
                    username: ConfigManager.Instance.Config.HaciendaClientConfig.Username,
                    password: ConfigManager.Instance.Config.HaciendaClientConfig.Password,
                    authorizationTokenUri: new Uri(ConfigManager.Instance.Config.HaciendaClientConfig.AuthorizationTokenUri),
                    apiBaseUri: new Uri(ConfigManager.Instance.Config.HaciendaClientConfig.ApiBaseUri),
                    fixedApiPath: ConfigManager.Instance.Config.HaciendaClientConfig.FixedApiPath
            );
            _facturaElectronica = ModelBuilder.Instance.GetFacturaElectronica();
            _clave = ConfigManager.Instance.Config.FacturaFixedDataConfig.Clave;

            SalirApplicationCommand = new RelayCommand(SalirApplication);
            EnviarHaciendaCommand = new RelayCommand(EnviarHacienda);
            AcercaDeCommand = new RelayCommand(AcercaDe);
            NuevaFacturaCommand = new RelayCommand(NuevaFactura);
            RevisarEnvioCommand = new RelayCommand(RevisarEnvio);
            SalvarOutputResultCommand = new RelayCommand(SalvarOutputResult);
            ReloadConfigCommand = new RelayCommand(ReloadConfig);
            SaveConfigCommand = new RelayCommand(SaveConfig);
        }

        public void ReloadConfig()
        {
            Trace.TraceInformation("BEGIN ReloadConfig");
            ConfigManager.Instance.LoadSettings();
            Trace.TraceInformation("END ReloadConfig");
        }

        public void SaveConfig()
        {
            Trace.TraceInformation("BEGIN SaveConfig");
            //ConfigManager.Instance.Config.FacturaFixedDataConfig.Clave = Clave;
            ConfigManager.Instance.SaveSettings();
            Trace.TraceInformation("END SaveConfig");
        }

        public void AcercaDe()
        {
            UIShellContainer.Instance.ShowMessageBox("Test para Factura Electronica. Hecho por Herber Madrigal (hfmad@hotmail.com).");
        }

        public void SalirApplication()
        {
            Trace.TraceInformation("BEGIN SalirApplication");
            Trace.Flush();
            System.Threading.Thread.Sleep(1000);
            Environment.Exit(0);
        }

        public void EnviarHacienda()
        {
            Task.Run(async () =>
            {
                Trace.TraceInformation("BEGIN EnviarHacienda");
                // Generates a factura electronica
                _facturaElectronica.Clave = Clave;

                // Validates the model
                var modelErrors = ServiceProvider.Instance.ModelValidator.GetErrorMessages(_facturaElectronica);
                if (modelErrors.Any())
                {
                    foreach (var errorMesage in modelErrors)
                    {
                        Trace.TraceInformation(errorMesage);
                    }
                }

                // Sends the  Factura to Hacienda's Server
                RecepcionComprobantesPostRequest recepcionComprobantesPostRequest = ModelBuilder.Instance.GetRecepcionComprobantes(clave: Clave);
                var comprobanteXML = ServiceProvider.Instance.XmlTextSerializer.Serialize(_facturaElectronica, new Dictionary<string, string>
                {
                    [string.Empty] = "https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/facturaElectronica",
                    [@"ds"] = System.Security.Cryptography.Xml.SignedXml.XmlDsigNamespaceUrl
                });
                recepcionComprobantesPostRequest.ComprobanteXml = ServiceProvider.Instance.TextEncoderService.ToBase64String(comprobanteXML);
                var recepcionComprobantesAsJson = recepcionComprobantesPostRequest.ToJson();
                var jsonValidationResult = ServiceProvider.Instance.JsonSchemeValidator.ValidateFileOrContent(recepcionComprobantesAsJson, Path.Combine(Environment.CurrentDirectory, $"Schemes{System.IO.Path.DirectorySeparatorChar}api.comprobanteselectronicos.go.cr-recepcion-v1-recepcion.json"));
                if (!jsonValidationResult.Item1)
                {
                    Trace.TraceError("Invalid JSON for Recepcion de Comprovantes");
                }

                await _haciendaClient.RefreshToken();
                await _haciendaClient.SendComprobantes(recepcionComprobantesPostRequest);
                Trace.TraceInformation("END EnviarHacienda");
                Trace.Flush();
            });

        }

        public void RevisarEnvio()
        {
            Task.Run(async () =>
            {
                Trace.TraceInformation("BEGIN RevisarEnvio");
                await _haciendaClient.RefreshToken();
                var resultadoEnvioComprobante = await _haciendaClient.GetResultadoEnvioDeComprobante(clave: Clave);
                var comprobanteRespuestaXml = ServiceProvider.Instance.TextEncoderService.FromBase64String(resultadoEnvioComprobante.RespuestaXml);
                var mensajeHacienda = ServiceProvider.Instance.XmlTextSerializer.Deserialize<MensajeHacienda>(comprobanteRespuestaXml);

                var stringBuilder = new StringBuilder();
                ServiceProvider.Instance.JsonTextSerializer.Serialize(mensajeHacienda, new StringWriter(stringBuilder));
                Trace.TraceInformation($"{stringBuilder}");
                Trace.TraceInformation("END RevisarEnvio");
                Trace.Flush();
            });
        }

        public void NuevaFactura()
        {
            Trace.TraceInformation("BEGIN NuevaFactura");
            _facturaElectronica = ModelBuilder.Instance.GetFacturaElectronica();
            _facturaElectronica.Clave = Clave;
            var stringBuilder = new StringBuilder();
            ServiceProvider.Instance.JsonTextSerializer.Serialize(_facturaElectronica, new StringWriter(stringBuilder));
            Trace.TraceInformation($"{stringBuilder}");
            Trace.TraceInformation("END NuevaFactura");
            Trace.Flush();
        }

        public void SalvarOutputResult()
        {
            Task.Run(async () =>
            {
                Trace.TraceInformation("BEGIN SalvarOutputResult");
                var outputFilePath = Path.Combine(Environment.CurrentDirectory, $"{DateTime.Now.ToString("o").Replace(":", "-")}.txt");
                using (var outputFileStream = File.OpenWrite(outputFilePath))
                using (var outputStreamWriter = new StreamWriter(outputFileStream))
                {
                    await outputStreamWriter.WriteLineAsync(UIShellContainer.Instance.OutputTextBuffer.Text);
                }

                UIShellContainer.Instance.ShowMessageBox($"Saved file:{Path.GetFileName(outputFilePath)}");
                Trace.TraceInformation("END SalvarOutputResult");
                Trace.Flush();


            });
        }

        internal string GetFacturaAsXml()
        {
            var facturaAsXml = ServiceProvider.Instance.XmlTextSerializer.Serialize(_facturaElectronica);
            return facturaAsXml;
        }

        internal string GetSignedDocument()
        {
            using (Stream outputSignedDocumentStream = new MemoryStream())
            {
                SignDocument(outputSignedDocumentStream);
                outputSignedDocumentStream.Seek(0, SeekOrigin.Begin);
                using (var streamReader = new StreamReader(outputSignedDocumentStream))
                {
                    var document = streamReader.ReadToEnd();
                    return document;
                }
            }
        }

        internal void SignDocument(Stream outputSignedDocumentStream)
        {
            using (var inputXmlDocumentStream = new MemoryStream())
            {
                // Generates XML into a stream
                ServiceProvider.Instance.XmlTextSerializer.Serialize(_facturaElectronica, inputXmlDocumentStream);
                inputXmlDocumentStream.Flush();
                inputXmlDocumentStream.Seek(0, SeekOrigin.Begin);

                // Signs the XML
                //System.Security.Cryptography.X509Certificates.X509Certificate2 certificate = ServiceProvider.Instance.XmlSigner.GetDefaultCertificat();
                System.Security.Cryptography.X509Certificates.X509Certificate2 certificate = new System.Security.Cryptography.X509Certificates.X509Certificate2("310167237828.p12", "1234");
                ServiceProvider.Instance.XmlSigner.SignSingleDocument(certificate, inputXmlDocumentStream, outputSignedDocumentStream);
            }

        }
    }

}
