﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.ConsoleApp.Models
{
    public class Config
    {
        public FacturaFixedDataConfig FacturaFixedDataConfig { get; set; }
        public HaciendaClientConfig HaciendaClientConfig { get; set; }
    }
    public class FacturaFixedDataConfig
    {
        public string Clave { get; set; }
    }
    public class HaciendaClientConfig
    {
        public string ClientId { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AuthorizationTokenUri { get; set; }
        public string ApiBaseUri { get; set; }
        public string FixedApiPath { get; set; }
    }
}
