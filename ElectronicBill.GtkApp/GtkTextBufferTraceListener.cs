﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.ConsoleApp
{
    public class GtkTextBufferTraceListener : System.Diagnostics.TraceListener
    {
        private TraceEventType _traceEventType;

        private Gtk.TextBuffer _textBuffer;

        private Dictionary<TraceEventType, Gtk.TextTag> _traceVentColorTable;

        public GtkTextBufferTraceListener(Gtk.TextBuffer textBuffer)
        {
            _traceVentColorTable = new Dictionary<TraceEventType, Gtk.TextTag>
            {
                [TraceEventType.Critical] = new Gtk.TextTag(TraceEventType.Critical.ToString()) { Foreground = "#cc3300" },
                [TraceEventType.Error] = new Gtk.TextTag(TraceEventType.Error.ToString()) { Foreground = "#990000" },
                [TraceEventType.Warning] = new Gtk.TextTag(TraceEventType.Warning.ToString()) { Foreground = "#ff9900" },
                [TraceEventType.Information] = new Gtk.TextTag(TraceEventType.Information.ToString()) { Foreground = "#000099" },
                [TraceEventType.Verbose] = new Gtk.TextTag(TraceEventType.Verbose.ToString()) { Foreground = "#003366" },
                [TraceEventType.Start] = new Gtk.TextTag(TraceEventType.Start.ToString()) { Foreground = "#009900" },
                [TraceEventType.Stop] = new Gtk.TextTag(TraceEventType.Stop.ToString()) { Foreground = "#009900" },
                [TraceEventType.Suspend] = new Gtk.TextTag(TraceEventType.Suspend.ToString()) { Foreground = "#009900" },
                [TraceEventType.Resume] = new Gtk.TextTag(TraceEventType.Resume.ToString()) { Foreground = "#009900" },
                [TraceEventType.Transfer] = new Gtk.TextTag(TraceEventType.Transfer.ToString()) { Foreground = "#009900" },
            };
            _textBuffer = textBuffer;
            foreach (var kvp in _traceVentColorTable)
            {
                _textBuffer.TagTable.Add(kvp.Value);
            }
        }

        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, object data)
        {
            _traceEventType = eventType;
            base.TraceData(eventCache, source, eventType, id, data);
        }
        public override void TraceData(TraceEventCache eventCache, string source, TraceEventType eventType, int id, params object[] data)
        {
            _traceEventType = eventType;
            base.TraceData(eventCache, source, eventType, id, data);
        }
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id)
        {
            _traceEventType = eventType;
            base.TraceData(eventCache, source, eventType, id);
        }
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string format, params object[] args)
        {
            _traceEventType = eventType;
            base.TraceData(eventCache, source, eventType, id, format, args);
        }
        public override void TraceEvent(TraceEventCache eventCache, string source, TraceEventType eventType, int id, string message)
        {
            _traceEventType = eventType;
            base.TraceData(eventCache, source, eventType, id, message);
        }

        public override void Write(string message)
        {
            AppendText($"{message}{Environment.NewLine}");
        }

        public override void WriteLine(string message)
        {
            AppendText($"{message}{Environment.NewLine}");
        }
        private void AppendText(string text)
        {
            Gtk.Application.Invoke(delegate
            {
                var textIterator = _textBuffer.EndIter;
                _textBuffer.InsertWithTags(ref textIterator, text, _traceVentColorTable[_traceEventType]);
            });
        }


    }
}
