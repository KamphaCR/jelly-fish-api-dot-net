﻿using ElectronicBill.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.ConsoleApp.Services
{
    public class ServiceProvider
    {


        internal static ServiceProvider Instance { get { return _instance.Value; } }

        private static readonly Lazy<ServiceProvider> _instance = new Lazy<ServiceProvider>(() => new ServiceProvider());
        private ServiceProvider() { }

        public ModelValidator ModelValidator
        {
            get
            {
                return _modelValidator;
            }
        }
        private readonly ModelValidator _modelValidator = new ModelValidator();

        public TextEncoderService TextEncoderService
        {
            get
            {
                return _textEncoderService;
            }
        }
        private readonly TextEncoderService _textEncoderService = new TextEncoderService();


        public XmlTextSerializer XmlTextSerializer
        {
            get
            {
                return _xmlTextSerializer;
            }
        }
        private readonly XmlTextSerializer _xmlTextSerializer = new XmlTextSerializer();

        public XmlSigner XmlSigner
        {
            get
            {
                return _xmlSigner;
            }
        }
        private readonly XmlSigner _xmlSigner = new XmlSigner();

        public XsdValidator XsdValidator
        {
            get
            {
                return _xsdValidator;
            }
        }
        private readonly XsdValidator _xsdValidator = new XsdValidator();

        public JsonSchemeValidator JsonSchemeValidator
        {
            get
            {
                return _jsonSchemeValidator;
            }
        }

        private readonly JsonSchemeValidator _jsonSchemeValidator = new JsonSchemeValidator();

        public JsonTextSerializer JsonTextSerializer
        {
            get
            {
                return _jsonTextSerializer;
            }
        }
        private readonly JsonTextSerializer _jsonTextSerializer = new JsonTextSerializer();

    }
}
