﻿using ElectronicBill.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ElectronicBill.ConsoleApp.Models;

namespace ElectronicBill.ConsoleApp.Services
{
    public class ConfigManager
    {

        internal static ConfigManager Instance { get { return _instance.Value; } }

        public Config Config
        {
            get
            {
                if (_config == null) { LoadSettings(); }
                return _config;
            }
        }
        private Models.Config _config;

        private static readonly Lazy<ConfigManager> _instance = new Lazy<ConfigManager>(() => new ConfigManager());

        private ConfigManager() { }

        private static string GetConfigFilePath()
        {
            var configFilePath = Path.Combine(Environment.CurrentDirectory, $"Config{System.IO.Path.DirectorySeparatorChar}Config.json");
            return configFilePath;
        }

		public void SetConfig(Config config)
		{
			if (config == Config)
				throw new ArgumentNullException (nameof(config));
			_config = config;
		}

        public void LoadSettings()
        {
            var configFilePath = GetConfigFilePath();
            var jsonTextSerializer = new JsonTextSerializer();
            using (var configStream = File.OpenRead(configFilePath))
            {
                _config = jsonTextSerializer.Deserialize<Models.Config>(configStream);
            }
        }

        public void SaveSettings()
        {
            var configFilePath = GetConfigFilePath();
            var jsonTextSerializer = new JsonTextSerializer();
            using (var configStream = File.Open(configFilePath,FileMode.Truncate))
            {
                jsonTextSerializer.Serialize(Config, configStream);
            }
        }
    }
}
