﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.ConsoleApp.Services
{
	public class GtkTextBufferTraceListener : System.Diagnostics.TraceListener
    {
        

        private StringBuilder _buffer;
        public GtkTextBufferTraceListener()
        {
            _buffer = new StringBuilder();
        }

        public override void Write(char value)
        {
			_buffer.Append(value);
            if (value == '\n')
            {
				var textBuffer = UIShellContainer.Instance.OutputTextBuffer;
				var textIterator = textBuffer.GetIterAtLine (textBuffer.LineCount);
				var tag = new Gtk.TextTag (null);
				textBuffer.InsertWithTags (ref textIterator, _buffer.ToString (), tag);
                //_listBox.Items.Add(_buffer.ToString());
                _buffer.Clear();
            }
            base.Write(value);
        }
    }
}
