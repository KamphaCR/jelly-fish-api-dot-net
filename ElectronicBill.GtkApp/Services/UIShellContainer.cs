﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Gtk;

namespace ElectronicBill.ConsoleApp.Services
{
    internal sealed class UIShellContainer
    {
        public Gtk.Window MainWindow { get; internal set; }
        public Gtk.TextBuffer OutputTextBuffer { get; internal set; }

        internal static UIShellContainer Instance { get { return _instance.Value; } }
        private static readonly Lazy<UIShellContainer> _instance = new Lazy<UIShellContainer>(() => new UIShellContainer());
        private UIShellContainer() { }

        public void ShowMessageBox(string format, params string[] args)
        {
            Gtk.Application.Invoke(delegate
            {
                MessageDialog md = new MessageDialog(MainWindow, DialogFlags.Modal, MessageType.Info, ButtonsType.Ok, format, args);
                md.Run();
                md.Destroy();
            });
        }

    }
}
