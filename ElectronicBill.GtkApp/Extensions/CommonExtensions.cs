﻿using System;
using System.Xml.Linq;

namespace ElectronicBill.GtkApp.Extensions
{
    public static class CommonExtensions
    {
        public static string GetBeautifierXml(this string xml)
        {
            try
            {
                XDocument doc = XDocument.Parse(xml);
                return doc.ToString();
            }
            catch (Exception)
            {
                return xml;
            }
        }
    }
}
