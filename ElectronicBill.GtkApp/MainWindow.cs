﻿using System;
using Gtk;
using ElectronicBill.ConsoleApp;
using ElectronicBill.ConsoleApp.Controllers;
using System.ComponentModel;
using ElectronicBill.ConsoleApp.Services;
using System.Text;
using ElectronicBill.ConsoleApp.Models;
using System.IO;
using System.Diagnostics;
using ElectronicBill.Extensions;
using ElectronicBill.GtkApp.Extensions;

public partial class MainWindow : Gtk.Window
{
    private MainController _mainController;

    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
        SetupController();
    }

    private void SetupController()
    {
        Trace.AutoFlush = true;
        if (Trace.Listeners.Count == 0)
        {
            if (Debugger.IsAttached)
            { Trace.Listeners.Add(Debug.Listeners[0]); }
            else
            { Trace.Listeners.Add(new ConsoleTraceListener()); }
        }
        //else
        //{ Trace.Listeners.Add(new DefaultTraceListener()); }
        Trace.Listeners.Add(new ElectronicBill.ConsoleApp.GtkTextBufferTraceListener(OutputTextView.Buffer));
        System.AppDomain.CurrentDomain.UnhandledException += (object sender, UnhandledExceptionEventArgs e) =>
        {
            var exception = e.ExceptionObject as Exception;
            if (exception == null)
                return;
            exception.TraceError();
        };
        System.Threading.Tasks.TaskScheduler.UnobservedTaskException += (object sender, System.Threading.Tasks.UnobservedTaskExceptionEventArgs e) =>
        {
            var exception = e.Exception;
            if (exception == null)
                return;
            exception.TraceError();
        };

        UIShellContainer.Instance.MainWindow = this;
        UIShellContainer.Instance.OutputTextBuffer = OutputTextView.Buffer;

        _mainController = new MainController();
        _mainController.PropertyChanged += OnMainControllerPropertyChangedEventHandler;
        ClaveEntry.Text = _mainController.Clave;


        CargarConfiguration();


    }

    private void CargarConfiguration()
    {
        _mainController.ReloadConfigCommand.Execute(_mainController);
        var sb = new StringBuilder();
        ServiceProvider.Instance.JsonTextSerializer.Serialize(ConfigManager.Instance.Config, sb);
        ConfigurationJsonTextView.Buffer.Text = sb.ToString();
        Trace.Flush();
    }

    protected void OnMainControllerPropertyChangedEventHandler(object sender, PropertyChangedEventArgs e)
    {
        switch (e.PropertyName)
        {
            case nameof(_mainController.Clave):
                ClaveEntry.Text = _mainController.Clave;
                break;
            default:
                break;
        }
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnVerificarFacturaButtonClicked(object sender, EventArgs e)
    {
        _mainController.RevisarEnvioCommand.Execute(_mainController);
        Trace.Flush();
    }

    protected void OnEnviarFacturaButtonClicked(object sender, EventArgs e)
    {
        _mainController.EnviarHaciendaCommand.Execute(_mainController);
        Trace.Flush();
    }

    protected void OnNuevaFacturaButtonClicked(object sender, EventArgs e)
    {
        _mainController.NuevaFacturaCommand.Execute(_mainController);
        Trace.Flush();
    }

    protected void OnClaveEntryChanged(object sender, EventArgs e)
    {
        var claveEntry = sender as Entry;
        _mainController.Clave = claveEntry.Text;
    }

    protected void OnAcercaDeButtonClicked(object sender, EventArgs e)
    {
        _mainController.AcercaDeCommand.Execute(_mainController);
        Trace.Flush();
    }

    protected void OnCargarConfigurationButtonClicked(object sender, EventArgs e)
    {
        CargarConfiguration();
        Trace.Flush();
    }

    protected void OnSalvarConfigurationButtonClicked(object sender, EventArgs e)
    {
        var config = ServiceProvider.Instance.JsonTextSerializer.Deserialize<Config>(new StringReader(ConfigurationJsonTextView.Buffer.Text));
        ConfigManager.Instance.SetConfig(config);
        _mainController.SaveConfigCommand.Execute(_mainController);
        Trace.Flush();
    }

    protected void OnClearOutputButtonClicked(object sender, EventArgs e)
    {
        Trace.TraceInformation("Clear Results");
        OutputTextView.Buffer.Clear();
        Trace.Flush();
    }

    protected void OnSaveOutputResultButtonClicked(object sender, EventArgs e)
    {
        _mainController.SalvarOutputResultCommand.Execute(_mainController);
    }

    protected void OnFlushOutputResultButtonClicked(object sender, EventArgs e)
    {
        Trace.Flush();
    }

    protected void OnShowFacturaXmlButtonClicked(object sender, EventArgs e)
    {
        //var signedXmlFactura = _mainController.GetSignedDocument();
        //facturaTextView.Buffer.Text = signedXmlFactura.GetBeautifierXml();

        var xmlFactura = _mainController.GetFacturaAsXml ();
        facturaTextView.Buffer.Text = xmlFactura.GetBeautifierXml();

    }
}
