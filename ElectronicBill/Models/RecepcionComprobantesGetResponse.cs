﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.Models
{
    public class RecepcionComprobantesGetResponse
    {
        public string clave { get; set; }
        public string fecha { get; set; }
        [JsonProperty("ind-estado")]
        public string IndicadorEstado { get; set; }
        [JsonProperty("respuesta-xml")]
        public string RespuestaXml { get; set; }

    }
}
