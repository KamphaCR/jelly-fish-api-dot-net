﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

// 
// This source code was auto-generated by xsd, Version=4.6.1055.0.
// 
namespace ElectronicBill.Models
{
    using System.Xml.Serialization;


    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/mensajeHacienda")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/mensajeHacienda", IsNullable = false)]
    public partial class MensajeHacienda
    {

        private string claveField;

        private string tipoIdentificacionEmisorField;

        private string numeroCedulaEmisorField;

        private string tipoIdentificacionReceptorField;

        private string numeroCedulaReceptorField;

        private string mensajeField;

        private string detalleMensajeField;

        /// <remarks/>
        public string Clave
        {
            get
            {
                return this.claveField;
            }
            set
            {
                this.claveField = value;
            }
        }

        /// <remarks/>
        public string TipoIdentificacionEmisor
        {
            get
            {
                return this.tipoIdentificacionEmisorField;
            }
            set
            {
                this.tipoIdentificacionEmisorField = value;
            }
        }

        /// <remarks/>
        public string NumeroCedulaEmisor
        {
            get
            {
                return this.numeroCedulaEmisorField;
            }
            set
            {
                this.numeroCedulaEmisorField = value;
            }
        }

        /// <remarks/>
        public string TipoIdentificacionReceptor
        {
            get
            {
                return this.tipoIdentificacionReceptorField;
            }
            set
            {
                this.tipoIdentificacionReceptorField = value;
            }
        }

        /// <remarks/>
        public string NumeroCedulaReceptor
        {
            get
            {
                return this.numeroCedulaReceptorField;
            }
            set
            {
                this.numeroCedulaReceptorField = value;
            }
        }

        /// <remarks/>
        public string Mensaje
        {
            get
            {
                return this.mensajeField;
            }
            set
            {
                this.mensajeField = value;
            }
        }

        /// <remarks/>
        public string DetalleMensaje
        {
            get
            {
                return this.detalleMensajeField;
            }
            set
            {
                this.detalleMensajeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/mensajeHacienda")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/mensajeHacienda", IsNullable = false)]
    public partial class NewDataSet
    {

        private MensajeHacienda[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("MensajeHacienda")]
        public MensajeHacienda[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }
}
