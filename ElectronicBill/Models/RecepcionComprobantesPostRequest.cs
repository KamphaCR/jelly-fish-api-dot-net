﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Collections.Specialized;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace ElectronicBill.Models
{
    public partial class RecepcionComprobantesPostRequest : INotifyPropertyChanged
    {
        private string _clave;
        private string _fecha;
        private Emisor _emisor;
        private Receptor _receptor;
        private string _callbackUrl;
        private string _consecutivoReceptor;
        private string _comprobanteXml;

        [JsonProperty("clave", Required = Required.Always)]
        public string Clave
        {
            get { return _clave; }
            set
            {
                if (_clave != value)
                {
                    _clave = value;
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>Fecha de la factura en formato [yyyy-MM-dd'T'HH:mm:ssZ] como se define en [http://tools.ietf.org/html/rfc3339#section-5.6] (date-time).</summary>
        [JsonProperty("fecha", Required = Required.Always)]
        public string Fecha
        {
            get { return _fecha; }
            set
            {
                if (_fecha != value)
                {
                    _fecha = value;
                    RaisePropertyChanged();
                }
            }
        }

        [JsonProperty("emisor", Required = Required.Always)]
        public Emisor Emisor
        {
            get { return _emisor; }
            set
            {
                if (_emisor != value)
                {
                    _emisor = value;
                    RaisePropertyChanged();
                }
            }
        }

        [JsonProperty("receptor", Required = Required.Default)]
        public Receptor Receptor
        {
            get { return _receptor; }
            set
            {
                if (_receptor != value)
                {
                    _receptor = value;
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>URL utilizado para que Hacienda envíe la respuesta de aceptación o rechazo, se va a enviar un mensaje JSON, igual al que se define en recepcionGetItem, por medio de un canal HTTP/HTTPS utilizando POST.</summary>
        [JsonProperty("callbackUrl", Required = Required.Default)]
        public string CallbackUrl
        {
            get { return _callbackUrl; }
            set
            {
                if (_callbackUrl != value)
                {
                    _callbackUrl = value;
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>Numeración consecutiva de los mensajes de confirmación. Este atributo es obligatorio en caso de ser un mensaje de confirmación del receptor.</summary>
        [JsonProperty("consecutivoReceptor", Required = Required.Default)]
        public string ConsecutivoReceptor
        {
            get { return _consecutivoReceptor; }
            set
            {
                if (_consecutivoReceptor != value)
                {
                    _consecutivoReceptor = value;
                    RaisePropertyChanged();
                }
            }
        }

        /// <summary>Comprobante electrónico XML firmado por el obligado tributario utilizando XAdES-EPES. El texto del XML debe convertirse a un byte array y codificarse en Base64. El mapa de caracteres a utilizar en el XML y en la codificación Base64 es UTF8.</summary>
        [JsonProperty("comprobanteXml", Required = Required.Always)]
        public string ComprobanteXml
        {
            get { return _comprobanteXml; }
            set
            {
                if (_comprobanteXml != value)
                {
                    _comprobanteXml = value;
                    RaisePropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static RecepcionComprobantesPostRequest FromJson(string data)
        {
            return JsonConvert.DeserializeObject<RecepcionComprobantesPostRequest>(data);
        }

        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public partial class Emisor : INotifyPropertyChanged
    {
        private string _tipoIdentificacion;
        private string _numeroIdentificacion;

        [JsonProperty("tipoIdentificacion", Required = Required.Always)]
        public string TipoIdentificacion
        {
            get { return _tipoIdentificacion; }
            set
            {
                if (_tipoIdentificacion != value)
                {
                    _tipoIdentificacion = value;
                    RaisePropertyChanged();
                }
            }
        }

        [JsonProperty("numeroIdentificacion", Required = Required.Always)]
        public string NumeroIdentificacion
        {
            get { return _numeroIdentificacion; }
            set
            {
                if (_numeroIdentificacion != value)
                {
                    _numeroIdentificacion = value;
                    RaisePropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static Emisor FromJson(string data)
        {
            return JsonConvert.DeserializeObject<Emisor>(data);
        }

        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }

    public partial class Receptor : INotifyPropertyChanged
    {
        private string _tipoIdentificacion;
        private string _numeroIdentificacion;

        [JsonProperty("tipoIdentificacion", Required = Required.Always)]
        public string TipoIdentificacion
        {
            get { return _tipoIdentificacion; }
            set
            {
                if (_tipoIdentificacion != value)
                {
                    _tipoIdentificacion = value;
                    RaisePropertyChanged();
                }
            }
        }

        [JsonProperty("numeroIdentificacion", Required = Required.Always)]
        public string NumeroIdentificacion
        {
            get { return _numeroIdentificacion; }
            set
            {
                if (_numeroIdentificacion != value)
                {
                    _numeroIdentificacion = value;
                    RaisePropertyChanged();
                }
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

        public static Receptor FromJson(string data)
        {
            return JsonConvert.DeserializeObject<Receptor>(data);
        }

        protected virtual void RaisePropertyChanged([CallerMemberName] string propertyName = null)
        {
            var handler = PropertyChanged;
            if (handler != null)
                handler(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}