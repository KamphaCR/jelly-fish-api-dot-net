﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill
{
    internal static class Constants
    {
        public static class MimeTypes
        {
            public const string ApplicationJson = @"application/json";
        }
        public static class Headers
        {
            public const string XErrorCause = "X-Error-Cause";
        }
    }
}
