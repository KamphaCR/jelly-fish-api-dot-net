﻿# ElectronicBill
## Resumen
El objetivo de de este proyecto es ofrecer una biblioteca en .NET que permita consumir los servicios para el envio de facturas digitales que ha expuesto el Ministerio de Hacienda de Costa Rica.

## Notas:
### Generación automática de modelos
Las siguientes archivos fueron autogenerados por medio de herramientas externas:

- `FacturaElectronica_FR_xmldsig-core-schema.cs`: generated using `XSD.exe` command
- `RecepcionComprobantesPostRequest.cs`: generated using http://json2csharp.rohitl.com/ (using the JSONScheme https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/comprobantes-electronicos-api.html#recepcion_post)
- `RecepcionComprobantesRespuesta.cs`: generated using XSD.exe (XSD file was generated using XSD.exe from the "RecepcionComprobantesGetResponse.RespuestaXml" document)

### Documentación del API de Facturación de Ministerio de Hacienda

#### Rapida referencia al API de hacienda
		https://tribunet.hacienda.go.cr/docs/esquemas/2016/v4.1/comprobantes-electronicos-api.html#

### Documentation about XML Signature

#### Ejemplo en código 
* FrameworkPeru/openinvoiceperu 
	
		https://github.com/FrameworkPeru/openinvoiceperu/blob/develop/OpenInvoicePeru/OpenInvoicePeru.Firmado/Certificador.cs
* FirmaXadesNet
		
		https://github.com/ctt-gob-es/FirmaXadesNet
		https://www.nuget.org/packages/FirmaXadesNet/

	xocapik/Xades-EPES-c-
		
		https://github.com/xocapik/Xades-EPES-c-/blob/master/XadesSignedXml%2Ccs

	facturaE con .NET y C#
		
		http://sviudes.blogspot.com/2010/05/facturae-con-net-y-c.html

	XAdES.NET Project
	
			http://xadesnet.codeplex.com/
			http://xadesnet.codeplex.com/wikipage?title=XMLDSIG%20Simple%20Signature&referringTitle=Home

	IKVM.NET Home Page
		
			http://www.ikvm.net/ 
			
	`IKVM.NET` is an implementation of Java for Mono and the Microsoft .NET Framework. It includes the following components:

	`luisgoncalves/xades4j` (XAdES4j is an high-level, configurable and extensible Java implementation of XML Advanced Electronic Signatures (XAdES 1.3.2 and 1.4.1). It enables producing, verifying and extending signatures in the main XAdES forms: XAdES-BES, XAdES-EPES, XAdES-T and XAdES-C.)
	
			https://github.com/luisgoncalves/xades4j

	Digital Signature Service : creation, extension and validation of advanced electronic signatures
	
			https://github.com/esig/dss
			https://joinup.ec.europa.eu/sd-dss/webapp-demo/home

	`libdigidocpp` Creating and signing a document (local signing)

			http://open-eid.github.io/libdigidocpp/manual.html#Creating


#### Documentation sobre XADES
* XAdES4j - EN

		https://prezi.com/06vyxbgohncv/xades4j-en/

* POLÍTICA DE FIRMA (versión 3.1) FORMATO FACTURAE
	
		http://www.facturae.gob.es/politica_de_firma_formato_facturae/politica_de_firma_formato_facturae_v3_1.pdf

* XAdES4j — a Java Library for XAdES Signature Services
	
		http://repositorio.ipl.pt/bitstream/10400.21/651/1/Disserta%C3%A7%C3%A3o_XAdES4j.pdf

* XAdES From Wikipedia, the free encyclopedia
	
		https://en.wikipedia.org/wiki/XAdES

* Modelo de implementación de mecanismos de firma digital
	
		http://repositorio.conicit.go.cr:8080/xmlui/bitstream/handle/123456789/117/Modelo%20de%20implementaci%C3%B3n%20de%20mecanismos%20de%20firma%20digital.pdf?sequence=1&isAllowed=y

#### Requerimientos segun hacienda

* _Signature format_ : XAdES-EPS 1.3.2 Superior
* _Packaging_ : Enveloped
* _Level_ : 
* _Digest algorithm_ : SHA256 , SHA512
* _Allow expired certificate_ : 


+#### Working with Certificates in Mono
+https://www.codeproject.com/Articles/162194/Certificates-to-DB-and-Back
+https://stackoverflow.com/questions/25918024/how-to-delete-remove-certificates-from-mono-certificate-stores-my-and-trust
+https://stackoverflow.com/questions/24170773/using-x509certificate2-on-mono-loading-with-both-public-and-private-key
