﻿using ElectronicBill.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using ElectronicBill.Extensions;

namespace ElectronicBill.Services
{
    public sealed class HaciendaClient
    {
        //public static ComprobantesElectronicosClient Instance
        //{
        //    get { return _instance.Value; }
        //}
        //private static readonly Lazy<ComprobantesElectronicosClient> _instance = new Lazy<ComprobantesElectronicosClient>(() => new ComprobantesElectronicosClient());
        //private ComprobantesElectronicosClient() { }

        private readonly string _clientId;
        private readonly Uri _authorizationTokenUri;
        private readonly Uri _apiBaseUri;
        private readonly string _username;
        private readonly string _password;
        private readonly string _fixedApiPath;

        private HaciendaToken _currentToken;

        public HaciendaClient(string clientId, string username, string password, Uri authorizationTokenUri, Uri apiBaseUri, string fixedApiPath = null)
        {
            _fixedApiPath = fixedApiPath ?? string.Empty;
            _clientId = clientId;
            _username = username;
            _password = password;
            _authorizationTokenUri = authorizationTokenUri;
            _apiBaseUri = apiBaseUri;
        }

        public async Task RefreshToken()
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = new Uri(_authorizationTokenUri.GetLeftPart(UriPartial.Authority));
            var request = new HttpRequestMessage(HttpMethod.Post, _authorizationTokenUri.PathAndQuery);
            var keyValues = new List<KeyValuePair<string, string>>();
            keyValues.Add(new KeyValuePair<string, string>("client_id", _clientId));
            keyValues.Add(new KeyValuePair<string, string>("grant_type", @"password"));
            keyValues.Add(new KeyValuePair<string, string>("username", _username));
            keyValues.Add(new KeyValuePair<string, string>("password", _password));
            request.Content = new FormUrlEncodedContent(keyValues);

            var response = await httpClient.SendAsync(request);
            response.EnsureSuccessStatusCode();

            _currentToken = await response.Content.ReadAsAsync<HaciendaToken>();

        }

        private HttpClient GetHttpClient()
        {
            var httpClient = new HttpClient();
            httpClient.BaseAddress = _apiBaseUri;
            httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", _currentToken.AccessToken);
            return httpClient;
        }

        public async Task GetComprobantes(int? offset = null, int? limit = null, string emisor = null, string receptor = null)
        {
            if (_currentToken == null)
                return;
            var httpClient = GetHttpClient();

            var query = HttpUtility.ParseQueryString(string.Empty);
            if (offset.HasValue) { query["offset"] = offset.Value.ToString(); }
            if (limit.HasValue) { query["limit"] = limit.Value.ToString(); }
            if (!string.IsNullOrWhiteSpace(emisor)) { query["emisor"] = emisor; }
            if (!string.IsNullOrWhiteSpace(receptor)) { query["receptor"] = receptor; }
            string queryString = $"?{query.ToString()}";
            queryString = queryString.Length == 1 ? string.Empty : queryString;

            var response = await httpClient.GetAsync($@"{_fixedApiPath}/comprobantes{queryString}");

            try { response.EnsureSuccessStatusCode(); }
            catch (HttpRequestException httpRequestException)
            {
                var xErrorCauseMessage = GetHeader(response, Constants.Headers.XErrorCause);
                httpRequestException.Data[Constants.Headers.XErrorCause] = xErrorCauseMessage;
                httpRequestException.TraceError();
                throw;
            }

            var content = await response.Content.ReadAsStringAsync();

        }

        public async Task SendComprobantes(RecepcionComprobantesPostRequest recepcionComprobantesPostRequest)
        {
            if (_currentToken == null)
                return;
            var httpClient = GetHttpClient();

            //var jsonContent = recepcionComprobantesPostRequest.ToJson();
            //var content = new StringContent(jsonContent, Encoding.UTF8, Constants.MimeTypes.ApplicationJson);
            //var response = await httpClient.PostAsync($@"/recepcion-stage/v1/recepcion", content);

            var response = await httpClient.PostAsJsonAsync($@"{_fixedApiPath}/recepcion", recepcionComprobantesPostRequest);
            try { response.EnsureSuccessStatusCode(); }
            catch (HttpRequestException httpRequestException)
            {
                var xErrorCauseMessage = GetHeader(response, Constants.Headers.XErrorCause);
                httpRequestException.Data[Constants.Headers.XErrorCause] = xErrorCauseMessage;
                httpRequestException.TraceError();
                throw;
            }

            var responseContent = await response.Content.ReadAsStringAsync();
        }

        public async Task<RecepcionComprobantesGetResponse> GetResultadoEnvioDeComprobante(string clave)
        {
            if (_currentToken == null)
                return null;
            var httpClient = GetHttpClient();

            var response = await httpClient.GetAsync($@"{_fixedApiPath}/recepcion/{clave}");
            try { response.EnsureSuccessStatusCode(); }
            catch (HttpRequestException httpRequestException)
            {
                var xErrorCauseMessage = GetHeader(response, Constants.Headers.XErrorCause);
                httpRequestException.Data[Constants.Headers.XErrorCause] = xErrorCauseMessage;
                httpRequestException.TraceError();
                throw;
            }

            var jsonTextSerializer = new JsonTextSerializer();
            var jsonInputStream = await response.Content.ReadAsStreamAsync();
            var recepcionComprobantesResponse = jsonTextSerializer.Deserialize<Models.RecepcionComprobantesGetResponse>(jsonInputStream);
            return recepcionComprobantesResponse;

        }

        private static IEnumerable<string> GetHeaders(HttpResponseMessage response, IEnumerable<string> headerNamss)
        {
            return from header in response.Headers
                   where headerNamss.Contains(header.Key)
                   select string.Join(Environment.NewLine, header.Value.Select(v => v.ToString()));
        }

        private static string GetHeader(HttpResponseMessage response, string headerNams)
        {
            return (from header in response.Headers
                    where header.Key == headerNams
                    select string.Join(Environment.NewLine, header.Value.Select(v => v.ToString())))
                    .FirstOrDefault();
        }
    }


    public class HaciendaToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        [JsonProperty("refresh_expires_in")]
        public string RefreshExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("id_token")]
        public string IdToken { get; set; }

        [JsonProperty("not-before-policy")]
        public string NotBeforePolicy { get; set; }

        [JsonProperty("session_state")]
        public string SessionState { get; set; }
    }
}
