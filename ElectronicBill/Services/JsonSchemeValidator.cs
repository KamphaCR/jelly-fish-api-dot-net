﻿using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.Services
{
    public class JsonSchemeValidator
    {
        public Tuple<bool, IEnumerable<dynamic>> ValidateFile(string jsonFilePath, string jsonSchemeFilePath)
        {
            var jsonContent = File.ReadAllText(jsonFilePath);
            var jsonSchemeContent = File.ReadAllText(jsonSchemeFilePath);
            Tuple<bool, IEnumerable<dynamic>> validationResult = ValidateContent(jsonContent, jsonSchemeContent);
            return validationResult;
        }

        private static Tuple<bool, IEnumerable<dynamic>> ValidateContent(string jsonContent, string jsonSchemeContent)
        {
            // load schema
            JSchema schema = JSchema.Parse(jsonContent);
            JToken json = JToken.Parse(jsonSchemeContent);

            // validate json
            IList<ValidationError> errors;
            bool isJsonValid = json.IsValid(schema, out errors);

            // return error messages and line info to the browser
            var validationResult = new Tuple<bool, IEnumerable<dynamic>>(isJsonValid, errors.Select(e => e.Message).ToList());
            return validationResult;
        }

        public Tuple<bool, IEnumerable<dynamic>> ValidateFileOrContent(string jsonFilePathOrContent, string jsonSchemeFilePathOrContent)
        {
            var jsonContent = File.Exists(jsonFilePathOrContent) ? File.ReadAllText(jsonFilePathOrContent) : jsonFilePathOrContent;
            var jsonSchemeContent = File.Exists(jsonSchemeFilePathOrContent) ? File.ReadAllText(jsonSchemeFilePathOrContent) : jsonSchemeFilePathOrContent;
            Tuple<bool, IEnumerable<dynamic>> validationResult = ValidateContent(jsonContent, jsonSchemeContent);
            return validationResult;
        }
    }
}
