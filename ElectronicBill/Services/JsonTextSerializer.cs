﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.Services
{
    public sealed class JsonTextSerializer
    {
		public T Deserialize<T>(Stream jsonInputStream)
        {
            using (var streamReader = new StreamReader(jsonInputStream))
			using (var jsonReader = new JsonTextReader(streamReader))
			{
				return Deserialize<T> (jsonReader);
			}
        }

		public T Deserialize<T>(TextReader jsonTextReader)
		{
			using (var jsonReader = new JsonTextReader(jsonTextReader))
			{
				return Deserialize<T> (jsonReader);
			}
		}

		private T Deserialize<T>(JsonReader jsonReader)
		{
			var jsonSerializer = GetJsonSerializer();
			var instanceOfT = jsonSerializer.Deserialize<T>(jsonReader);
			return instanceOfT;
		}

        public void Serialize<T>(T instance, Stream jsonOutputStream)
        {
            var jsonSerializer = GetJsonSerializer();
            using (var streamWriter = new StreamWriter(jsonOutputStream))
            using (var jsonWriter = new JsonTextWriter(streamWriter))
            {
                jsonSerializer.Serialize(jsonWriter, instance);
                jsonWriter.Flush();
            }
        }

        public void Serialize<T>(T instance, TextWriter jsonTextWriter)
        {
            
            using (var jsonWriter = new JsonTextWriter(jsonTextWriter))
            {
                jsonWriter.Formatting = Formatting.Indented;
				var jsonSerializer = GetJsonSerializer();
                jsonSerializer.Serialize(jsonWriter, instance);
                jsonWriter.Flush();
            }
        }

		public void Serialize<T>(T instance, StringBuilder stringBuilder)
		{
			using (var stringWriter = new StringWriter (stringBuilder))
				Serialize (instance, stringWriter);
		}

        private static JsonSerializer GetJsonSerializer()
        {
            var jsonSerializer =  new JsonSerializer();
            return jsonSerializer;
        }
    }
}
