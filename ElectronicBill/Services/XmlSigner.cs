﻿using System;
using System.Linq;
using FirmaXadesNet;
using FirmaXadesNet.Signature.Parameters;
using FirmaXadesNet.Crypto;
using System.IO;
using System.Security.Cryptography.X509Certificates;

namespace ElectronicBill.Services
{
    public sealed class XmlSigner
    {
        //public static XmlSigner Instance
        //{
        //    get { return _instance.Value; }
        //}
        //private static readonly Lazy<XmlSigner> _instance = new Lazy<XmlSigner>(() => new XmlSigner());
        //private XmlSigner() { }

        public System.Security.Cryptography.X509Certificates.X509Certificate2 GetDefaultCertificate()
        {
            return FirmaXadesNet.Utils.CertUtil.SelectCertificate();
        }

        public void SignSingleDocument(X509Certificate2 certificate, Stream documentInputStream, Stream signedDocumentoutputStream)
        {
            XadesService xadesService = new XadesService();
            SignatureParameters signatureParameters = new SignatureParameters();

            // Política de firma de factura-e 3.1
            signatureParameters.SignaturePolicyInfo = new SignaturePolicyInfo();
            //signatureParameters.SignaturePolicyInfo.PolicyIdentifier = "http://www.facturae.es/politica_de_firma_formato_facturae/politica_de_firma_formato_facturae_v3_1.pdf";
            //signatureParameters.SignaturePolicyInfo.PolicyHash = "Ohixl6upD6av8N7pEvDABhEL6hM=";
            signatureParameters.SignaturePackaging = SignaturePackaging.ENVELOPED;
            signatureParameters.InputMimeType = System.Net.Mime.MediaTypeNames.Text.Xml; ;
            signatureParameters.SignerRole = new SignerRole();
            //signatureParameters.SignerRole.ClaimedRoles.Add("emisor");

            using (signatureParameters.Signer = new Signer(certificate))
            {
                var signedDocument = xadesService.Sign(documentInputStream, signatureParameters);
                signedDocument.Save(signedDocumentoutputStream);
                signedDocumentoutputStream.Flush();
            }

        }
    }
}
