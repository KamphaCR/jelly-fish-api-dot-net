﻿using System;
using FizzWare.NBuilder;
using ElectronicBill.Models;
using System.Linq;

namespace ElectronicBill.Services
{
    public sealed partial class ModelBuilder
    {
        private readonly static Random _random = new Random();

        public static ModelBuilder Instance
        {
            get { return _instance.Value; }
        }
        private static readonly Lazy<ModelBuilder> _instance = new Lazy<ModelBuilder>(() => new ModelBuilder());

        private ModelBuilder()
        { }

        public FacturaElectronica GetFacturaElectronica()
        {

            var facturaElectronicaBuilder = Builder<FacturaElectronica>
                .CreateNew()
                .With(x => x.Clave = Faker.StringFaker.Numeric(50))
                .With(x => x.NumeroConsecutivo = Faker.StringFaker.Numeric(20))
                .With(x => x.Emisor = GetEmisorType())
                .With(x => x.Receptor = GetReceptorType())
                .With(x => x.CondicionVenta = Faker.EnumFaker.SelectFrom<FacturaElectronicaCondicionVenta>())
                .With(x => x.PlazoCredito = Faker.StringFaker.Alpha(_random.Next(1, 10)))
                .With(x => x.MedioPago = Enumerable.Range(0, 3).Select(i => Faker.EnumFaker.SelectFrom<FacturaElectronicaMedioPago>()).Distinct().ToArray())
                .With(x => x.DetalleServicio = Enumerable.Range(0, _random.Next(10)).Select(i => GetFacturaElectronicaLineaDetalle()).ToArray())
                .With(x => x.ResumenFactura = GetFacturaElectronicaResumenFactura())
                .With(x => x.InformacionReferencia = Enumerable.Range(0, _random.Next(1, 10)).Select(i => GetFacturaElectronicaInformacionReferencia()).ToArray())
                .With(x => x.Normativa = GetFacturaElectronicaNormativa())
                .With(x => x.Otros = GetFacturaElectronicaOtros())
                //Signature
                //.With(x => x.Signature = GetSignatureType())
                ;
            var facturaElectronica = facturaElectronicaBuilder.Build();
            return facturaElectronica;
        }

        public FacturaElectronicaOtros GetFacturaElectronicaOtros()
        {
            var facturaElectronicaOtrosBuilder = Builder<FacturaElectronicaOtros>
                .CreateNew()
                //.With(x => x.OtroTexto = Enumerable.Range(0, _random.Next(1, 10)).Select(i => GetFacturaElectronicaOtrosOtroTexto()).ToArray())
                //.With(x => x.OtroContenido = Enumerable.Range(0, _random.Next(1, 10)).Select(i => GetFacturaElectronicaOtrosOtroContenido()).ToArray())
                ;
            var facturaElectronicaOtros = facturaElectronicaOtrosBuilder.Build();
            return facturaElectronicaOtros;
        }

        public FacturaElectronicaOtrosOtroContenido GetFacturaElectronicaOtrosOtroContenido()
        {
            var facturaElectronicaOtrosOtroContenidoBuilder = Builder<FacturaElectronicaOtrosOtroContenido>
                .CreateNew()
                .With(x => x.codigo = Faker.StringFaker.AlphaNumeric(10))
                //.With(x => x.Any = System.Xml.XmlElement)
                ;
            var facturaElectronicaOtrosOtroContenido = facturaElectronicaOtrosOtroContenidoBuilder.Build();
            return facturaElectronicaOtrosOtroContenido;
        }

        public FacturaElectronicaOtrosOtroTexto GetFacturaElectronicaOtrosOtroTexto()
        {
            var facturaElectronicaOtrosOtroTextoBuilder = Builder<FacturaElectronicaOtrosOtroTexto>
                .CreateNew()
                .With(x => x.codigo = Faker.StringFaker.AlphaNumeric(10))
                .With(x => x.Value = Faker.StringFaker.AlphaNumeric(50))
                ;
            var facturaElectronicaOtrosOtroTexto = facturaElectronicaOtrosOtroTextoBuilder.Build();
            return facturaElectronicaOtrosOtroTexto;
        }

        public FacturaElectronicaNormativa GetFacturaElectronicaNormativa()
        {
            var facturaElectronicaNormativaBuilder = Builder<FacturaElectronicaNormativa>
                .CreateNew()
                .With(x => x.NumeroResolucion = Faker.StringFaker.Alpha(_random.Next(1, 13)))
                .With(x => x.FechaResolucion = Faker.DateTimeFaker.DateTime().ToString("dd-MM-yyyy HH:mm:ss"))
                ;
            var facturaElectronicaNormativa = facturaElectronicaNormativaBuilder.Build();
            return facturaElectronicaNormativa;
        }

        public FacturaElectronicaInformacionReferencia GetFacturaElectronicaInformacionReferencia()
        {
            var facturaElectronicaInformacionReferenciaBuilder = Builder<FacturaElectronicaInformacionReferencia>
                .CreateNew()
                .With(x => x.TipoDoc = Faker.EnumFaker.SelectFrom<FacturaElectronicaInformacionReferenciaTipoDoc>())
                .With(x => x.Numero = Faker.StringFaker.AlphaNumeric(_random.Next(1, 50)))
                .With(x => x.FechaEmision = Faker.DateTimeFaker.DateTime())
                .With(x => x.Codigo = Faker.EnumFaker.SelectFrom<FacturaElectronicaInformacionReferenciaCodigo>())
                .With(x => x.Razon = Faker.StringFaker.AlphaNumeric(_random.Next(1, 180)))
                ;
            var facturaElectronicaInformacionReferencia = facturaElectronicaInformacionReferenciaBuilder.Build();
            return facturaElectronicaInformacionReferencia;
        }

        public FacturaElectronicaResumenFactura GetFacturaElectronicaResumenFactura()
        {
            var facturaElectronicaResumenFacturaBuilder = Builder<FacturaElectronicaResumenFactura>
                .CreateNew()
                .With(x => x.CodigoMoneda = Faker.EnumFaker.SelectFrom<FacturaElectronicaResumenFacturaCodigoMoneda>())
                .With(x => x.TipoCambio = GetDecimalDineroType())
                .With(x => x.TotalServGravados = GetDecimalDineroType())
                .With(x => x.TotalMercanciasGravadas = GetDecimalDineroType())
                .With(x => x.TotalGravado = GetDecimalDineroType())
                .With(x => x.TotalExento = GetDecimalDineroType())
                .With(x => x.TotalVenta = GetDecimalDineroType())
                .With(x => x.TotalDescuentos = GetDecimalDineroType())
                .With(x => x.TotalVentaNeta = GetDecimalDineroType())
                .With(x => x.TotalImpuesto = GetDecimalDineroType())
                .With(x => x.TotalComprobante = GetDecimalDineroType())
                ;
            var facturaElectronicaResumenFactura = facturaElectronicaResumenFacturaBuilder.Build();
            return facturaElectronicaResumenFactura;
        }

        public FacturaElectronicaLineaDetalle GetFacturaElectronicaLineaDetalle()
        {
            var facturaElectronicaLineaDetalleBuilder = Builder<FacturaElectronicaLineaDetalle>
                .CreateNew()
                .With(x => x.NumeroLinea = Faker.NumberFaker.Number(0, int.MaxValue).ToString())
                //.With(x => x.Codigo = Enumerable.Range(0, _random.Next(10)).Select(i => GetCodigoType()).ToArray())
                .With(x => x.Cantidad = decimal.Parse($"{Faker.StringFaker.Numeric(_random.Next(1, 13))}.{_random.Next(1, 999).ToString("000")}"))
                .With(x => x.UnidadMedida = Faker.EnumFaker.SelectFrom<UnidadMedidaType>())
                .With(x => x.UnidadMedidaComercial = Faker.StringFaker.AlphaNumeric(_random.Next(1, 20)))
                .With(x => x.Detalle = Faker.StringFaker.AlphaNumeric(_random.Next(1, 160)))
                .With(x => x.PrecioUnitario = GetDecimalDineroType())
                .With(x => x.MontoTotal = GetDecimalDineroType())
                .With(x => x.MontoDescuento = GetDecimalDineroType())
                .With(x => x.NaturalezaDescuento = Faker.StringFaker.AlphaNumeric(_random.Next(1, 80)))
                .With(x => x.SubTotal = GetDecimalDineroType())
                .With(x => x.Impuesto = Enumerable.Range(0, _random.Next(1, 4)).Select(i => GetImpuestoType()).ToArray())
                .With(x => x.MontoTotalLinea = GetDecimalDineroType())
                ;
            var facturaElectronicaLineaDetalle = facturaElectronicaLineaDetalleBuilder.Build();
            return facturaElectronicaLineaDetalle;
        }

        public ImpuestoType GetImpuestoType()
        {
            var impuestoTypeBuilder = Builder<ImpuestoType>
                .CreateNew()
                .With(x => x.Codigo = Faker.EnumFaker.SelectFrom<ImpuestoTypeCodigo>())
                .With(x => x.Tarifa = decimal.Parse($"{Faker.StringFaker.Numeric(_random.Next(1, 3))}.{_random.Next(1, 99).ToString("00")}"))
                .With(x => x.Monto = GetDecimalDineroType())
                ;
            var impuestoType = impuestoTypeBuilder.Build();
            return impuestoType;
        }

        public ExoneracionType GetExoneracionType()
        {
            var exoneracionTypeBuilder = Builder<ExoneracionType>
                .CreateNew()
                .With(x => x.TipoDocumento = Faker.EnumFaker.SelectFrom<ExoneracionTypeTipoDocumento>())
                .With(x => x.NumeroDocumento = Faker.StringFaker.AlphaNumeric(_random.Next(1, 17)))
                .With(x => x.NombreInstitucion = Faker.StringFaker.AlphaNumeric(_random.Next(1, 100)))
                .With(x => x.FechaEmision = Faker.DateTimeFaker.DateTime())
                .With(x => x.MontoImpuesto = GetDecimalDineroType())
                .With(x => x.PorcentajeCompra = _random.Next(1, 100).ToString())
                ;
            var exoneracionType = exoneracionTypeBuilder.Build();
            return exoneracionType;
        }

        public CodigoType GetCodigoType()
        {
            var codigoTypeBuilder = Builder<CodigoType>
                .CreateNew()
                .With(x => x.Tipo = Faker.EnumFaker.SelectFrom<CodigoTypeTipo>())
                .With(x => x.Codigo = Faker.StringFaker.AlphaNumeric(_random.Next(1, 19)))
                ;
            var codigoType = codigoTypeBuilder.Build();
            return codigoType;
        }

        private ReceptorType GetReceptorType()
        {
            var receptorTypeTypeBuilder = Builder<ReceptorType>
                .CreateNew()
                .With(x => x.Nombre = Faker.StringFaker.Alpha(_random.Next(1, 80)))
                .With(x => x.Identificacion = GetIdentificacionType())
                .With(x => x.IdentificacionExtranjero = Faker.StringFaker.Alpha(_random.Next(1, 20)))
                .With(x => x.NombreComercial = Faker.StringFaker.Alpha(_random.Next(1, 80)))
                .With(x => x.Ubicacion = GetUbicacionType())
                .With(x => x.Fax = GetTelefonoType())
                .With(x => x.CorreoElectronico = Faker.InternetFaker.Email())
                ;
            var receptorType = receptorTypeTypeBuilder.Build();
            return receptorType;
        }

        public EmisorType GetEmisorType()
        {
            var emisorTypeBuilder = Builder<EmisorType>
                .CreateNew()
                .With(x => x.Nombre = Faker.StringFaker.Alpha(_random.Next(1, 80)))
                .With(x => x.Identificacion = GetIdentificacionType())
                .With(x => x.NombreComercial = Faker.StringFaker.Alpha(_random.Next(1, 80)))
                .With(x => x.Ubicacion = GetUbicacionType())
                .With(x => x.Telefono = GetTelefonoType())
                .With(x => x.Fax = GetTelefonoType())
                .With(x => x.CorreoElectronico = Faker.InternetFaker.Email())
                ;
            var emisorType = emisorTypeBuilder.Build();
            return emisorType;
        }

        public IdentificacionType GetIdentificacionType()
        {
            var identificacionTypeBuilder = Builder<IdentificacionType>
                .CreateNew()
                .With(x => x.Numero = Faker.StringFaker.Numeric(_random.Next(9, 12)))
                .With(x => x.Tipo = Faker.EnumFaker.SelectFrom<IdentificacionTypeTipo>())
                ;
            var identificacionType = identificacionTypeBuilder.Build();
            return identificacionType;
        }

        public UbicacionType GetUbicacionType()
        {
            var UbicacionTypeBuilder = Builder<UbicacionType>
                .CreateNew()
                .With(x => x.Provincia = Faker.StringFaker.Numeric(1))
                .With(x => x.Canton = Faker.NumberFaker.Number(0, 99).ToString("00"))
                .With(x => x.Distrito = Faker.NumberFaker.Number(0, 99).ToString("00"))
                .With(x => x.Barrio = Faker.NumberFaker.Number(0, 99).ToString("00"))
                .With(x => x.OtrasSenas = $"{Faker.LocationFaker.StreetName()} {Faker.LocationFaker.ZipCode()})")
                ;
            var ubicacionType = UbicacionTypeBuilder.Build();
            return ubicacionType;
        }

        public TelefonoType GetTelefonoType()
        {
            var telefonoTypeBuilder = Builder<TelefonoType>
                .CreateNew()
                .With(x => x.CodigoPais = Faker.StringFaker.Numeric(_random.Next(1, 3)))
                .With(x => x.NumTelefono = Faker.StringFaker.Numeric(_random.Next(1, 20)))
                ;
            var telefonoType = telefonoTypeBuilder.Build();
            return telefonoType;
        }

        public decimal GetDecimalDineroType()
        {
            return decimal.Parse($"{Faker.StringFaker.Numeric(_random.Next(0, 12))}.{_random.Next(0, 99999).ToString("00000")}");
        }

        //public SignatureType GetSignatureType()
        //{
        //    var signatureTypeBuilder = Builder<SignatureType>
        //        .CreateNew()
        //        .With(x => x)
        //        ;
        //    var signatureType = signatureTypeBuilder.Build();
        //    return signatureType;
        //}

    }

    public sealed partial class ModelBuilder
    {
        public RecepcionComprobantesPostRequest GetRecepcionComprobantes(string clave = null)
        {
            var recepcionComprobantesPostRequestBuilder = Builder<RecepcionComprobantesPostRequest>
                .CreateNew()
                .With(x => x.Clave = clave ?? string.Empty)
                .With(x => x.CallbackUrl = Faker.InternetFaker.Url())
                .With(x => x.Fecha = DateTime.Now.ToString("o"))
                .With(x => x.Emisor = GetEmisor())
                .With(x => x.Receptor = GetReceptor())
                ;
            var recepcionComprobantesPostRequest = recepcionComprobantesPostRequestBuilder.Build();
            return recepcionComprobantesPostRequest;
        }

        private Receptor GetReceptor()
        {
            var receptorBuilder = Builder<Receptor>
                .CreateNew()
                .With(x => x.TipoIdentificacion = "02")
                .With(x => x.NumeroIdentificacion = Faker.StringFaker.Numeric(_random.Next(9, 12)))
                ;

            var receptor = receptorBuilder.Build();
            return receptor;
        }

        private Emisor GetEmisor()
        {
            var emisorBuilder = Builder<Emisor>
                .CreateNew()
                .With(x => x.TipoIdentificacion = "02")
                .With(x => x.NumeroIdentificacion = Faker.StringFaker.Numeric(_random.Next(9, 12)))
                ;
            var emisor = emisorBuilder.Build();
            return emisor;
        }
    }
}

