﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Schema;

namespace ElectronicBill.Services
{
    public sealed class XsdValidator
    {
        //public static XsdValidator Instance
        //{
        //    get { return _instance.Value; }
        //}
        //private static readonly Lazy<XsdValidator> _instance = new Lazy<XsdValidator>(() => new XsdValidator());


        public void Validate(string xmlFilPath, IEnumerable<KeyValuePair<string, string>> schemeNamespaceAndFilePaths)
        {

            // Set the validation settings.
            XmlReaderSettings settings = new XmlReaderSettings();
            XmlSchemaSet schemaSet = new XmlSchemaSet();
            foreach (var schemeNamespaceAndFilePath in schemeNamespaceAndFilePaths)
            {
                if (System.IO.File.Exists(schemeNamespaceAndFilePath.Value))
                {
                    schemaSet.Add(
                        schemeNamespaceAndFilePath.Key,
                        XmlReader.Create(System.IO.File.OpenRead(schemeNamespaceAndFilePath.Value),
                            new XmlReaderSettings() { DtdProcessing = DtdProcessing.Parse }
                        )
                    );
                }
                else
                {
                    schemaSet.Add(schemeNamespaceAndFilePath.Key, schemeNamespaceAndFilePath.Value);
                }
            }
            settings.Schemas.Add(schemaSet);
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;
            settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
            settings.ValidationEventHandler += new ValidationEventHandler(ValidationCallBack);

            // Create the XmlReader object.
            using (XmlReader reader = XmlReader.Create(xmlFilPath, settings))
            {
                // Parse the file. 
                while (reader.Read()) ;
            }

                
            settings.ValidationEventHandler -= ValidationCallBack;
        }

        //Display any warnings or errors.
        private static void ValidationCallBack(object sender, ValidationEventArgs args)
        {
            switch (args.Severity)
            {
                case XmlSeverityType.Error:
                    Trace.TraceError($"{args.Message}");
                    break;
                case XmlSeverityType.Warning:
                    Trace.TraceWarning($"{args.Message}");
                    break;
                default:
                    Trace.TraceInformation($"{args.Message}");
                    break;
            }
        }

    }
}
