﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace ElectronicBill.Services
{
    public sealed class XmlTextSerializer
    {
        //public static XmlTextSerializer Instance
        //{
        //    get { return _instance.Value; }
        //}
        //private static readonly Lazy<XmlTextSerializer> _instance = new Lazy<XmlTextSerializer>(() => new XmlTextSerializer());

        //private XmlTextSerializer()
        //{ }

        public void Serialize<T>(T instance, Stream outputStream, IEnumerable<KeyValuePair<string, string>> xmlNamespaces = null)
        {
            var xmlSerializer = new XmlSerializer(typeof(T), string.Empty);
            if (xmlNamespaces?.Any() == true)
            {

                xmlSerializer.Serialize(outputStream, instance,
                    new XmlSerializerNamespaces(
                        xmlNamespaces.Select(xmlns => new XmlQualifiedName(xmlns.Key, xmlns.Value)).ToArray()
                ));
            }
            else
            {
                xmlSerializer.Serialize(outputStream, instance);
            }
            outputStream.Flush();
        }

        public void Serialize<T>(T instance, TextWriter textWriter, IEnumerable<KeyValuePair<string, string>> xmlNamespaces = null)
        {
            var xmlSerializer = new XmlSerializer(typeof(T), string.Empty);
            if (xmlNamespaces?.Any() == true)
            {

                xmlSerializer.Serialize(textWriter, instance,
                    new XmlSerializerNamespaces(
                        xmlNamespaces.Select(xmlns => new XmlQualifiedName(xmlns.Key, xmlns.Value)).ToArray()
                ));
            }
            else
            {
                xmlSerializer.Serialize(textWriter, instance);
            }
            textWriter.Flush();
        }

        public void Serialize<T>(T instance, string targetFilePath, IEnumerable<KeyValuePair<string, string>> xmlNamespaces = null)
        {
            using (var streamWriter = File.CreateText(targetFilePath))
            {
                Serialize(instance, streamWriter, xmlNamespaces);
            }
        }

        public string Serialize<T>(T instance, IEnumerable<KeyValuePair<string, string>> xmlNamespaces = null)
        {
            var stringBuilder = new StringBuilder();
            using (var stringWriter = new StringWriter(stringBuilder))
                Serialize(instance, stringWriter, xmlNamespaces);
            return stringBuilder.ToString();
        }

        public T Deserialize<T>(string xmlText)
        {
            using (var stringReader = new StringReader(xmlText))
                return Deserialize<T>(stringReader);
        }

        public T Deserialize<T>(TextReader xmlTextReader)
        {
            var xmlSerializer = new XmlSerializer(typeof(T), string.Empty);
            var instance = (T)xmlSerializer.Deserialize(xmlTextReader);
            return instance;
        }

        public T Deserialize<T>(Stream xmlInputStream)
        {
            var xmlSerializer = new XmlSerializer(typeof(T), string.Empty);
            var instance = (T)xmlSerializer.Deserialize(xmlInputStream);
            return instance;
        }
    }
}
