﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.Services
{
    public class TextEncoderService
    {
        public string ToBase64String(string input, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            byte[] bytes = encoding.GetBytes(input);
            string base64Text = Convert.ToBase64String(bytes);
            return base64Text;
        }

        public string FromBase64String(string base64Input, Encoding encoding = null)
        {
            encoding = encoding ?? Encoding.UTF8;
            byte[] bytes = Convert.FromBase64String(base64Input);
            string decodedString = encoding.GetString(bytes);
            return decodedString;
        }
    }
}
