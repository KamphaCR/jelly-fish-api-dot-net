﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentValidation.Results;
using FluentValidation;
using ElectronicBill.Models;

namespace ElectronicBill.Services
{
    public sealed class ModelValidator
    {
        //public static ModelValidator Instance
        //{
        //    get { return _instance.Value; }
        //}
        //private static readonly Lazy<ModelValidator> _instance = new Lazy<ModelValidator>(() => new ModelValidator());

        //private ModelValidator()
        //{ }

        public IEnumerable<string> GetErrorMessages(Models.FacturaElectronica facturaElectronica)
        {
            var validator = new FacturaElectronicaValidator();
            validator.CascadeMode = FluentValidation.CascadeMode.Continue;
            var validationResult = validator.Validate(facturaElectronica);

            var validationSucceeded = validationResult.IsValid;
            var failures = validationResult.Errors;
            var errorMessages = failures.Select(failure => failure.ErrorMessage);
            return errorMessages;
        }


    }


    #region Validators
    public class FacturaElectronicaValidator : AbstractValidator<FacturaElectronica>
    {
        public FacturaElectronicaValidator()
        {
            RuleFor(facturaElectronica => facturaElectronica.Emisor)
                .NotNull()
                .SetValidator(new EmisorValidator() { CascadeMode = CascadeMode });
            RuleFor(facturaElectronica => facturaElectronica.Clave)
                .NotNull()
                .Matches(@"\d{20}");
            RuleFor(facturaElectronica => facturaElectronica.NumeroConsecutivo)
                .NotNull()
                .Matches(@"\d{20}");

        }
    }

    public class EmisorValidator : AbstractValidator<EmisorType>
    {
        public EmisorValidator()
        {
            RuleFor(emisor => emisor.Ubicacion)
                .NotNull()
                .SetValidator(new UbicacionValidator() { CascadeMode = CascadeMode });
            RuleFor(emisor => emisor.Identificacion)
                .NotNull().
                SetValidator(new IdentificacionValidator() { CascadeMode = CascadeMode });
            RuleFor(emisor => emisor.Nombre)
                .NotNull()
                .Must(nombre => !string.IsNullOrWhiteSpace(nombre))
                .Length(1, 80);
            RuleFor(emisor => emisor.NombreComercial).Length(0, 80);
            RuleFor(emisor => emisor.CorreoElectronico).EmailAddress();
        }
    }

    public class IdentificacionValidator : AbstractValidator<IdentificacionType>
    {
        public IdentificacionValidator()
        {
            RuleFor(identificacion => identificacion.Tipo)
                .IsInEnum();
            RuleFor(identificacion => identificacion.Numero)
                .Matches(@"\d{9,12}");
        }
    }

    public class UbicacionValidator : AbstractValidator<UbicacionType>
    {
        public UbicacionValidator()
        {
            RuleFor(ubicacion => ubicacion.Provincia)
                .Matches(@"\d");
            RuleFor(ubicacion => ubicacion.Canton)
                .Matches(@"\d\d");
            RuleFor(ubicacion => ubicacion.Distrito)
                .Matches(@"\d\d");
            RuleFor(ubicacion => ubicacion.Barrio)
                .Matches(@"\d\d");
            RuleFor(ubicacion => ubicacion.OtrasSenas)
                .Length(0, 160);
        }
    }

    public class TelefonoValidator : AbstractValidator<TelefonoType>
    {
        public TelefonoValidator()
        {
            RuleFor(telefono => telefono.CodigoPais)
                .Must(codigoPais => { int i; return int.TryParse(codigoPais, out i); })
                .Length(1, 3);
            RuleFor(telefono => telefono.NumTelefono)
                .Must(numeroDeTelefono => { int i; return int.TryParse(numeroDeTelefono, out i); })
                .Length(1, 20);
        }
    }
    #endregion
}
