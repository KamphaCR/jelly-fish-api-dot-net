﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElectronicBill.Extensions
{
    public static class ExceptionExtensions
    {
        public static void TraceError(this Exception exception)
        {
            var exceptionData = string.Join(Environment.NewLine,
                from key in exception.Data.Keys.OfType<object>()
                select $"\t[{key}]={exception.Data[key]}");
            Trace.TraceError($"Message:{Environment.NewLine}\t{exception.Message}{Environment.NewLine}StackTrace:{Environment.NewLine}{(exception.StackTrace??string.Empty).Replace(@"   at","\tat")}{Environment.NewLine}Data:{Environment.NewLine}{exceptionData}");
            Trace.Flush();
        }
    }
}
